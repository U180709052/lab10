package stack;

import java.util.ArrayList;

public class StackArrayImpl implements Stack {
	
    ArrayList<StackItem> stack=new ArrayList<StackItem>();

    @Override
    public void push(Object item) {		
	stack.add(new StackItem(item,null));		
    }
    @Override
    public Object pop() {
        return null;
    }

    @Override
    public boolean empty() {
	return false;
    }
}
